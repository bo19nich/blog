from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, SubmitField, PasswordField
from wtforms.validators import Required, Email

class LoginForm(Form):
	email = SubmitField('Email', validators=[Required(), length(1, 64), Email()])
	password = PasswordField('Password', validators=[Required()])
	remember_me=BooleanField('Keep Me Logged In')
	submit = SubmitField('Submit')